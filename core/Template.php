<?php
    namespace playtown\panel;

    class Template{

        private function openTemplate($template,$content=[]){
           $template = "../templates/{$template}.html";

            if (!file_exists($template)){
                # TODO: logueo error?
                return false;
            }

           $html = null;
           $template_handler = fopen($template,"r");

           while (!feof($template_handler)){
                $line = fgets($template_handler);
                if ($content){
                    foreach ($content as $clave=>$valor){
                        $line = str_replace("{{{$clave}}}",$valor,$line);
                    }
                }
                $html .= $line;
           }

           fclose($template_handler);
           return $html;
        }

        public static function show($template,$content=[]){
            echo self::openTemplate($template,$content);
        }

    }