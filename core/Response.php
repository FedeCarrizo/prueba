<?php

    namespace playtown\panel;

    class Response
    {
        public static function json($arr,$http_code = 200){
            self::out($arr,$http_code);
        }

        public static function error($arr){
            self::out($arr,500);
        }

        public static function custom($arr,$http_code){
            self::out($arr,$http_code);
        }

        private function out($arr,$http_code){
            @header('Content-Type: application/json');
            http_response_code($http_code);
            die(json_encode(self::utf8ize($arr)));
        }

        public static function utf8ize($d) {
            if (is_array($d)) {
                foreach ($d as $k => $v) {
                    $d[$k] = self::utf8ize($v);
                }
            } else if (is_string ($d)) {
                return utf8_encode($d);
            }
            return $d;
        }
    }