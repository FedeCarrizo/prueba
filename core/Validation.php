<?php

namespace playtown\panel;

    class Validation
    {
        public static function make($arr){
            foreach ($arr as $needed){
                if (!Input::has($needed)) {
                    return false;
                }
            }
            return true;
        }
    }