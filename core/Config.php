<?php

    namespace playtown\panel;

    class Config
    {
        private function returnSetting($arr,$key,$def=''){
            if (isset($arr[$key])){
                return $arr[$key];
            }
            return $def;
        }
    
        public static function app($key,$def=''){
            $arr = include('../config/app.php');
            return self::returnSetting($arr, $key, $def);

        }

        static function db($key,$def=''){
            $arr = include('../config/db.php');
            return self::returnSetting($arr,$key,$def);
        }
    }