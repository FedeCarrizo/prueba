<?php

    namespace playtown\panel;

    abstract class Model{

        protected $_fields = [];

        public function get($id){
            $result = $GLOBALS['DB']->select("SELECT * FROM {$this->_db}.{$this->_table} WHERE {$this->_primary} = {$id}");
            return $result;
        }

        public function delete($id){
            $result = $GLOBALS['DB']->delete("DELETE FROM {$this->_db}.{$this->_table} WHERE {$this->_primary} = {$id}");
            return true;
        }


        public static function create($d){
            // TODO armar la funcion create
        }

        public static function all(){
            $clase = static::class;
            $result = $GLOBALS['DB']->select("SELECT * FROM {$clase::$_db}.{$clase::$_table} ");
            return $result;
        }

        public function save(){

            $campos = null;

            $propiedades = get_object_vars($this);
            foreach ($propiedades as $p=>$v){

                # ignoro las propiedades del modelo
                if (substr($p,0,1) == "_"){
                    continue;
                }

                # verifico que existan en la tabla las propiedades
                if (!isset($this->_fields[$p])){
                    throw new \Exception("No existe el campo {$p} en la tabla {$this->_table}");
                }

                # armo el query
                $campos .= "{$p} = '{$v}' ,";

            }

            $campos = rtrim($campos,',');
            $query = "UDPATE {$this->_db}.{$this->_table} SET {$campos} WHERE {$this->_primary} = {$this->{$this->_primary}} ";

            $result = $GLOBALS['DB']->update($query);
            return $result;
        }

        public function __set($propiedad, $valor){
            $this->{strtolower($propiedad)} = $valor;
        }

        public function __construct($id){
            $clase = static::class;

            $result = $GLOBALS['DB']->select("SELECT * FROM {$clase::$_db}.{$clase::$_table} WHERE {$clase::$_primary} = ?",[$id]);

            foreach ($result[0] as $clave=>$valor){
                $clave = strtolower($clave);
                if (!$valor){
                    $this->_fields[$clave] = '';
                    $this->{$clave} = '';
                }else{
                    $this->_fields[$clave] = $valor;
                    $this->{$clave} = $valor;
                }
            }

            return $result;
        }

    }