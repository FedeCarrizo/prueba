<?php

    namespace playtown\panel;

    /**
     * Class Input
     * @package playtown\wap
     */
    class Input
    {
        public static function all(){
            # TODO: foreach y serializar?
            return $_GET + $_POST + $_COOKIE;
        }

        public static function has($key)
        {
            if (isset($_REQUEST[$key])) {
                return true;
            }

            return false;
        }

        public static function get($key, $default = '')
        {
            if (!self::has($key)) {
                return $default;
            }

            return $_REQUEST[$key];
        }
    }