<?php

    namespace playtown\panel;
    use \PDO;

    class Database extends PDO{

        private $db = false;
        var $lastE;

        function __call($func,$args){
            if(!in_array($func, array("select", "update", "delete", "insert"))){
                Response::error(['error'=>'invalid method']);
            }

            try{
                if (count($args) == 2){
                    $stmt = $this->db->prepare($args[0]);
                    $stmt->execute($args[1]);
                }else{
                    $stmt = $this->db->query($args[0]);
                }

            }catch(\PDOException $e){
                die($e);

                $this->lastE = $e;
                return NULL;
            }
            
            if($func == 'select'){
                return $stmt->fetchAll();
            }

            if($func == 'insert'){
                echo $this->lastE;
                return $this->db->lastInsertId();
            }

            return $stmt->rowCount();
        }

        function __construct(){
            $host 		= Config::db('db_hostname');
            $user 		= Config::db('db_username');
            $pass 		= Config::db('db_password');
            $base 		= Config::db('db_database');

            try{
                $this->db 	= new PDO('mysql:host='.$host.';dbname='.$base, $user, $pass,[
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
                ]);

                $this->update("SET NAMES utf8");

            }catch (PDOException $e){
                Wanno::error(['error'=>$e->getMessage()]);
            }

            return $this->db;
        }
    }