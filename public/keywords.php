<?php

    namespace playtown\panel;

    /**
     * Autoload.
     */
    require "../init.php";

    /**
     * Validaciones
     */

    $_action = null;

    if (!Input::has('action')){
        Response::custom(['error'=>'invalid request'],400);
    }else{
        $_action = Input::get('action');
    }

    $_allowed = ['get','all','create','delete'];

    if (!in_array($_action,$_allowed)){
        Response::json(['error'=>'method not allowed'],400);
    }


    /**
     * Magia
     */

    switch ($_action){
        case 'get':
            if (!Input::has('id')){
                Response::json(['error'=>'invalid request'],400);
            }

            $keyword = new Keyword(Input::get('id'));

            if (!$keyword){
                Response::json(['error'=>'invalid keyword'],400);
            }

            Response::json($keyword);
            break;

        case 'edit':
            if (!Input::has('id')){
                Response::json(['error'=>'invalid request'],400);
            }

            $keyword = new Keyword(Input::get('id'));

            if (!$keyword){
                Response::json(['error'=>'invalid keyword'],400);
            }
            

            $keyword->nombre        = Input::get('nombre',$keyword->nombre);
            $keyword->id_producto   = Input::get('nombre',$keyword->id_producto);
            $keyword->fecha_desde   = Input::get('nombre',$keyword->fecha_desde);
            $keyword->fecha_hasta   = Input::get('nombre',$keyword->fecha_hasta);

            if ($keyword->save()){
                Response::json(['success'=>1]);
            }
            break;

        case 'delete':
            if (!Input::has('id')){
                Response::json(['error'=>'invalid request'],400);
            }

            $keyword = new Keyword(Input::get('id'));
            $keyword->delete(Input::get('id'));


            Response::json(['success'=>1]);
            break;


        case 'all':
            Response::json(Keyword::all());
            break;

        case 'create':
            if (!Validation::make(['keyword','id_producto'])){
                Response::json(['error'=>'invalid request'],400);
            }

            $newKeyword = Keyword::create([
                'keyword'       => Input::get('keyword'),
                'id_producto'   => Input::get('id_producto'),
            ]);

            if (!$newKeyword){
                Response::error(['error'=>'error creating keyword']);
            }

            Response::json($newKeyword);

            break;
        default:
            Response::json(['error'=>'invalid request'],400);
    }

